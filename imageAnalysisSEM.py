#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 25 15:11:15 2018

@author: joe
"""

import skimage.io as io
from skimage.morphology import watershed
from skimage.measure import regionprops
from skimage.segmentation import find_boundaries
from skimage.filters import sobel
import os
import re
import numpy as np
from matplotlib import pyplot as plt

def listAllSEMImages( mydir='' ):
    if mydir:
        os.chdir(mydir)
    allFiles = os.listdir()
    textCheck = re.compile('(.+)(\.txt)')
    imageCheck = re.compile('(.+)\.(jpg||tif||bmp)')
    
    imageList = []
    textList = []
    
    for i in allFiles:
        isText = textCheck.match(i)
        if isText:
            #print(isText.group(1))
            textList.append(isText.group(1))
            
        isImage = imageCheck.match(i)
        if isImage:
            #print(isImage.group(1))
            imageList.append(isImage.group(1))
            
    #print(imageList)
    #print(textList)
            
    for i in imageList:
        if i not in textList:
            print(i + " is not in textList!")
            imageList.remove(i)
            
    return imageList

def loadTextToDict( textName ):
    sideFinder = re.compile('(\w+)=(\w+.\w+)||(\w+)')
    textDict = {}
    with open(textName+'.txt', 'r') as f:
        for row in f:
            #print(row)
            m = sideFinder.match(row)
            if m:
                textDict[m.group(1)] = m.group(2)
                
    return textDict            

class imageM:
    '''Image with metadata'''
    
    def __init__(self, name):
        self.meta = loadTextToDict( name )
        self.im = io.imread(name + '.' + self.meta['Format'])
        
    def toMicrons(self, pixels):
        return pixels*float(self.meta['PixelSize'])/1e3
        
        
def watersheder( image ):
    elevation  = sobel(image)
    
    markers = np.zeros(image.shape)
    markers[0,:] = 1
    markers[-1,:] = 1
    markers[:,0] = 1
    markers[:,-1] = 1
    
    s = (int(np.floor(markers.shape[0]/5)),int(np.floor(markers.shape[1]/5)))
    markers[ 2*s[0]:-2*s[0],2*s[1]:-2*s[1]] = 2
    
    segments = watershed(elevation, markers)
    bounds = find_boundaries(segments, mode = 'thick')
    rprops = regionprops(segments)[1]
    
    center = (int(round(rprops.centroid[0])),int(round(rprops.centroid[1])))
    
    indices = np.indices(image.shape)#row is 0, col is 1
    xvals = np.array(indices[0][bounds] - center[0])
    yvals = np.array(indices[1][bounds] - center[1])
    
    distances = np.sqrt(xvals**2 + yvals**2)
    angles = np.arctan2(xvals, yvals)
    
    return distances, angles, bounds

def fastRadPlot(ang, dist):
    ax = plt.subplot(111, projection = 'polar')
    ax.plot(ang,dist,'.')
    ax.set_rmin(dist.min())
    plt.show()
    
def overlayPlot(originalImage, bounds):
    return 0